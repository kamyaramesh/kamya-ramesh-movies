/**
 * 
 */
package com.thiraineram.helpers.bo;

import java.util.ArrayList;


import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.thiraineram.bo.Movie;
import com.thiraineram.entity.*;



/**
 * @author Kamya
 *
 */
@Service
public class MovieHelper {
	
	
	public ArrayList<Movie> getMovies() {
		
		ArrayList<Movie> movies;
		
		Movie movie1 = new Movie();
		movie1.setName("LUCY");
		movie1.setRating("PG-13");
		movie1.setType("Action/Thriller");
		movie1.setDuration("1 hr 30 min");
		ArrayList<String> cast1 = new ArrayList<>();
		cast1.add("Scarlet Johansson"); 
		cast1.add("Morgan Freeman");
		movie1.setCast(cast1);
		movie1.setSynopsis("The story centers on a woman (Johansson) who falls prey to sinister underworld forces, but who gains superhuman abilities that allow her to strike back at her oppressors.");
		movie1.setIs2D(true);
		movie1.setIs3D(false);
		
		
		Movie movie2 = new Movie();
		movie2.setName("GUARDIANS OF THE GALAXY");
		movie2.setRating("PG-13");
		movie2.setType("Action/Thriller");
		movie2.setDuration("1 hr 30 min");
		ArrayList<String> cast2 = new ArrayList<>();
		cast2.add("Benicio Del Toro"); 
		cast2.add("Chris Pratt");
		cast2.add("Zoe Saldana");
		cast2.add("Ophelia lovibond");
		movie2.setCast(cast2);
		movie2.setSynopsis("A group of interstellar outlaws team up to save the galaxy from a villain who seeks ultimate power in this comic book space adventure from Marvel Studios and director James Gunn (Slither, Super). Peter Quill was just a young boy when, devastated by his mother's death, he sprinted out of the hospital and was swept into the stars by Yondu (Michael Rooker), the leader of an eclectic band of space scavengers known as The Ravagers. Twenty-six years later, Quill has adopted the nickname Star Lord (Chris Pratt). He's broken away from The Ravagers in an attempt to track down an ancient orb that is also coveted by the evil Ronan (Lee Pace), who is in league with the dreaded Thanos, and who dispatches his top assassin Gamora (Zoe Saldana) to retrieve it from Star Lord. In the process of doing so, Gamora also gets drawn into a fight with furry bounty hunter Rocket (voice of Bradley Cooper) and his tree-like, humanoid companion Groot (voice of Vin Diesel). Subsequently thrown into prison, this unlikely quartet quickly makes the acquaintance of fearsome warrior Drax the Destroyer (Dave Bautista), who seeks bitter revenge for the slaughter of his entire family at the hands of Ronan. In the process of making a daring prison break, Star Lord, Gamora, Rocket, Groot, and Drax recover the orb, and transport it to The Collector (Benecio Del Toro), who reveals that it houses an Infinity Gem - one of six precious stones that harbors incredible powers that could pose a dire threat should they fall into the wrong hands. Later, Ronan acquires the Infinity Gem and plots to use its power to destroy The Nova Corps home-world of Xandar, The Guardians of the Galaxy must race through the stars to recover it before Ronan can carry out his genocidal plan. ");
		movie2.setIs2D(true);
		movie2.setIs3D(true);
		
		Movie movie3 = new Movie();
		movie3.setName("HERCULES");
		movie3.setRating("PG-13");
		movie3.setType("Action/Thriller");
		movie3.setDuration("1 hr 30 min");
		ArrayList<String> cast3 = new ArrayList<>();
		cast3.add("Dwayne Johnson"); 
		cast3.add("Ian McShane");
		cast3.add("Rufus Sewell");
		cast3.add("John Hurt");
		cast3.add("Rebecca Ferguson");
		movie3.setCast(cast3);
		movie3.setSynopsis("As the story opens, a haunted Hercules (Johnson) travels Greece with his five loyal companions, capitalizing on his heroic reputation by selling his services for gold. When a malevolent warlord threatens to thrust the kingdom of Thrace into chaos, however, its desperate ruler and his daughter turn to Hercules to restore the peace. In order to succeed, Hercules must once again summon the strength and valor that once made him a myth among men. Joseph Fiennes, Ian McShane, and Rufus Sewell co-star.");
		movie3.setIs2D(true);
		movie3.setIs3D(true);
		
		Movie movie4 = new Movie();
		movie4.setName("DAWN OF THE PLANET OF THE APES");
		movie4.setRating("PG-13");
		movie4.setType("Action/Thriller");
		movie4.setDuration("1 hr 30 min");
		ArrayList<String> cast4 = new ArrayList<>();
		cast4.add("Gary Oldman"); 
		cast4.add("Keri Russell");
		cast4.add("Judy Greer");
		cast4.add("Kodi Sit-McPhee");
		movie4.setCast(cast4);

		movie4.setSynopsis("A war begins to brew between man and ape in this sequel to the 2011 hit Rise of the Planet of the Apes. It's been ten years since the Simian Flu wiped out most of humanity, and somewhere deep in the woods outside of San Francisco, Caesar (voice and performance capture by Andy Serkis) and his primate companions have established a thriving village built on the principles of peace and community. Shortly after welcoming a baby brother into the family, Caesar's son Blue Eyes (Nick Thurston) is walking through the forest with his friend Ash (Doc Shaw) when they cross paths with a human named Carver (Kirk Acevedo), who impulsively draws his gun and shoots Ash at the first sign of aggression. As it turns out, Carver is part of a human expedition led by Malcolm (Jason Clarke), who, along with the rest of his crew, races to Carver's side just as Caesar and the rest of the apes answer Blue Eyes' desperate call for help. An enraged Caesar drives the humans away after realizing they are no longer a threat, and decides to dispatch a small crew to follow them rather than yield to the pleas of his aggressive advisor Koba (Toby Kebbell) to launch an all-out attack. Instead, he decides to show the apes' strength by amassing outside of the humans' makeshift community at the base of an unfinished tower, making it unmistakably clear that the two species should remain apart."
				+ "Meanwhile, the point of the human excursion was to get a dormant dam running again in order to power their community, which will soon be thrust into darkness should they fail to take action. Convinced that he could strike a truce with Caesar that would allow the humans to repair the dam, which is located on the apes' land, Malcolm gets permission from human leader Dreyfus (Gary Oldman) to set out on his mission. Incredibly, thanks to the help of his girlfriend Ellie (Keri Russell), his son Alexander (Kodi Smit-McPhee), and a few other key allies, Malcolm succeeds. Still, his truce with the apes is a fragile one, and just when it seems that the primates and humans have learned to coexist in peace, a shocking act of betrayal threatens to spark a war that will determine the dominant species.");
		movie4.setIs2D(true);
		movie4.setIs3D(true);
		
		Movie movie5 = new Movie();
		movie5.setName("PLANES: FIRE AND RESCUE");
		movie5.setRating("PG-13");
		movie5.setType("Action/Thriller");
		movie5.setDuration("1 hr 30 min");
		ArrayList<String> cast5 = new ArrayList<>();
		cast5.add("Dane Cook"); 
		cast5.add("Julie Bowen");
		movie5.setCast(cast5);
		movie5.setSynopsis("Adventure flies higher than ever before in this sequel that finds championship air-racer Dusty Crophopper (voice of Dane Cook) beginning an exciting new career fighting forest fires. In the wake of another successful racing season, Dusty returns home to Propwash Junction to resume his training. But a debilitating injury forces Rusty to consider a new career path. Before long Rusty is training with the Aerial Fire Fighters at Piston Peak Air Attack Base. Becoming a Single Engine Air Tanker (SEAT) is no simple task for an injured plane, but with the guidance of seasoned fire-and-rescue helicopter Blade Ranger (voice of Ed Harris), fearless leader of the Piston Peak Air Attack team, Rusty is soon soaring through the clouds once again. Meanwhile, as Rusty gets up to speed, his presence in the unit catches the attention of Dipper (voice of Julie Bowen), a cargo hauler-turned-water scooper capable of skimming lake water to combat raging fires. A major racing fan, Dipper can hardly contain her excitement at the prospect of working alongside Rusty, and meanwhile wise, heavy-lift helicopter Windlifter (voice of Wes Studi) has never encountered a fire he couldn't conquer. In no time Rusty has made a whole host of new friends at Piston Peak, including hulking transport plane Cabbie (voice of Dale Dye), the brave grounded firefighters known as the Smokejumpers, the Secretary of the Interior (voice of Fred Willard), and Nick Lopez (voice of Erik Estrada), a helicopter cop with an illustrious past. And though learning the secrets to dousing forest fires is sure to take time, Rusty is a fast learner, and eager to pitch in.");
		movie5.setIs3D(true);
		movie5.setIs2D(false);
		
		movies = new ArrayList<>();
		movies.add(movie1);
		movies.add(movie2);
		movies.add(movie3);
		movies.add(movie4);
		movies.add(movie5);
		
		return movies;
		
	}

}
