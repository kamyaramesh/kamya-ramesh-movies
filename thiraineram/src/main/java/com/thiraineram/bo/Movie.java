/**
 * 
 */
package com.thiraineram.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.thiraineram.helpers.bo.MovieHelper;

/**
 * @author Kamya
 * 08/09/2014
 *
 */
public class Movie {

	private String name;
	private String rating;
	private String type;
	private String duration;
	private String synopsis;
	private ArrayList<String> cast;
	private boolean is3D;
	private boolean is2D;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSynopsis() {
		return synopsis;
	}
	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}
	
	public ArrayList<String> getCast() {
		return cast;
	}
	public void setCast(ArrayList<String> cast) {
		this.cast = cast;
	}
	public boolean isIs3D() {
		return is3D;
	}
	public void setIs3D(boolean is3d) {
		is3D = is3d;
	}
	public boolean isIs2D() {
		return is2D;
	}
	public void setIs2D(boolean is2d) {
		is2D = is2d;
	}
	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	
	
}
