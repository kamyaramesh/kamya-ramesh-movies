SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Movie`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Movie` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Movie` (
  `Movie_Id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(2000) NOT NULL,
  `rating` VARCHAR(100) NOT NULL,
  `type` VARCHAR(1000) NULL,
  `duration` VARCHAR(100) NOT NULL,
  `synopsis` VARCHAR(4000) NOT NULL,
  `is3D` TINYINT(1) NOT NULL,
  `is2D` TINYINT(1) NOT NULL,
  `year` INT NOT NULL,
  `awards` VARCHAR(2000) NULL,
  PRIMARY KEY (`Movie_Id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `Movie_Id_UNIQUE` ON `mydb`.`Movie` (`Movie_Id` ASC);


-- -----------------------------------------------------
-- Table `mydb`.`Theatre`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Theatre` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Theatre` (
  `Theatre_Id` INT NOT NULL,
  `name` VARCHAR(2000) NOT NULL COMMENT 'Theatre table has details about a theatre.',
  `zipcode` VARCHAR(45) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `state` VARCHAR(45) NOT NULL,
  `hasIMAX` TINYINT(1) NOT NULL,
  PRIMARY KEY (`Theatre_Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Movie_has_Theatre`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Movie_has_Theatre` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Movie_has_Theatre` (
  `Movie_Movie_Id` INT NOT NULL,
  `Theatre_Theatre_Id` INT NOT NULL,
  `showtimes` VARCHAR(45) NOT NULL,
  `is3D` TINYINT(1) NOT NULL,
  PRIMARY KEY (`Movie_Movie_Id`, `Theatre_Theatre_Id`),
  CONSTRAINT `fk_Movie_has_Theatre_Movie1`
    FOREIGN KEY (`Movie_Movie_Id`)
    REFERENCES `mydb`.`Movie` (`Movie_Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Movie_has_Theatre_Theatre1`
    FOREIGN KEY (`Theatre_Theatre_Id`)
    REFERENCES `mydb`.`Theatre` (`Theatre_Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Movie_has_Theatre_Theatre1_idx` ON `mydb`.`Movie_has_Theatre` (`Theatre_Theatre_Id` ASC);

CREATE INDEX `fk_Movie_has_Theatre_Movie1_idx` ON `mydb`.`Movie_has_Theatre` (`Movie_Movie_Id` ASC);


-- -----------------------------------------------------
-- Table `mydb`.`Artist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Artist` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Artist` (
  `Artist_Id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(1000) NOT NULL,
  `age` VARCHAR(45) NOT NULL,
  `role` VARCHAR(1000) NOT NULL,
  `gender` VARCHAR(45) NOT NULL,
  `nationality` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`Artist_Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Movie_has_Artist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Movie_has_Artist` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Movie_has_Artist` (
  `Movie_Movie_Id` INT NOT NULL,
  `Artist_Artist_Id` INT NOT NULL,
  `Awards` VARCHAR(2000) NULL,
  `Award nominations` VARCHAR(2000) NULL,
  PRIMARY KEY (`Movie_Movie_Id`, `Artist_Artist_Id`),
  CONSTRAINT `fk_Movie_has_Artist_Movie1`
    FOREIGN KEY (`Movie_Movie_Id`)
    REFERENCES `mydb`.`Movie` (`Movie_Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Movie_has_Artist_Artist1`
    FOREIGN KEY (`Artist_Artist_Id`)
    REFERENCES `mydb`.`Artist` (`Artist_Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Movie_has_Artist_Artist1_idx` ON `mydb`.`Movie_has_Artist` (`Artist_Artist_Id` ASC);

CREATE INDEX `fk_Movie_has_Artist_Movie1_idx` ON `mydb`.`Movie_has_Artist` (`Movie_Movie_Id` ASC);


-- -----------------------------------------------------
-- Table `mydb`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`User` ;

CREATE TABLE IF NOT EXISTS `mydb`.`User` (
  `User_Id` INT NOT NULL AUTO_INCREMENT,
  `user_Type` VARCHAR(45) NOT NULL,
  `emp_id` VARCHAR(45) NULL,
  `theater_user_id` VARCHAR(45) NULL,
  `email_id` VARCHAR(100) NOT NULL,
  `username` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`User_Id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `mydb`.`Movie`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`Movie` (`Movie_Id`, `name`, `rating`, `type`, `duration`, `synopsis`, `is3D`, `is2D`, `year`, `awards`) VALUES (1, 'Lucy', 'PG-18', 'Action/Thriller', '1 hr 30 min', 'The story centers on a woman (Johansson) who falls prey to sinister underworld forces, but who gains superhuman abilities that allow her to strike back at her oppressors.', 1, 1, 2014, NULL);
INSERT INTO `mydb`.`Movie` (`Movie_Id`, `name`, `rating`, `type`, `duration`, `synopsis`, `is3D`, `is2D`, `year`, `awards`) VALUES (2, 'GUARDIANS OF THE GALAXY', 'PG-13', 'Action/Thriller', '1 hr 30 min', 'A group of interstellar outlaws team up to save the galaxy from a villain who seeks ultimate power in this comic book space adventure from Marvel Studios and director James Gunn (Slither, Super). Peter Quill was just a young boy when, devastated by his mother\'s death, he sprinted out of the hospital and was swept into the stars by Yondu (Michael Rooker), the leader of an eclectic band of space scavengers known as The Ravagers. Twenty-six years later, Quill has adopted the nickname Star Lord (Chris Pratt). He\'s broken away from The Ravagers in an attempt to track down an ancient orb that is also coveted by the evil Ronan (Lee Pace), who is in league with the dreaded Thanos, and who dispatches his top assassin Gamora (Zoe Saldana) to retrieve it from Star Lord. In the process of doing so, Gamora also gets drawn into a fight with furry bounty hunter Rocket (voice of Bradley Cooper) and his tree-like, humanoid companion Groot (voice of Vin Diesel). Subsequently thrown into prison, this unlikely quartet quickly makes the acquaintance of fearsome warrior Drax the Destroyer (Dave Bautista), who seeks bitter revenge for the slaughter of his entire family at the hands of Ronan. In the process of making a daring prison break, Star Lord, Gamora, Rocket, Groot, and Drax recover the orb, and transport it to The Collector (Benecio Del Toro), who reveals that it houses an Infinity Gem - one of six precious stones that harbors incredible powers that could pose a dire threat should they fall into the wrong hands. Later, Ronan acquires the Infinity Gem and plots to use its power to destroy The Nova Corps home-world of Xandar, The Guardians of the Galaxy must race through the stars to recover it before Ronan can carry out his genocidal plan.', 1, 1, 2014, NULL);
INSERT INTO `mydb`.`Movie` (`Movie_Id`, `name`, `rating`, `type`, `duration`, `synopsis`, `is3D`, `is2D`, `year`, `awards`) VALUES (3, 'HERCULES', 'PG-13', 'Action/Thriller', '1 hr 30 min', 'As the story opens, a haunted Hercules (Johnson) travels Greece with his five loyal companions, capitalizing on his heroic reputation by selling his services for gold. When a malevolent warlord threatens to thrust the kingdom of Thrace into chaos, however, its desperate ruler and his daughter turn to Hercules to restore the peace. In order to succeed, Hercules must once again summon the strength and valor that once made him a myth among men. Joseph Fiennes, Ian McShane, and Rufus Sewell co-star.', 1, 1, 2014, NULL);
INSERT INTO `mydb`.`Movie` (`Movie_Id`, `name`, `rating`, `type`, `duration`, `synopsis`, `is3D`, `is2D`, `year`, `awards`) VALUES (4, 'DAWN OF THE PLANET OF THE APES', 'PG-13', 'Action/Adventure', '1 hr 30 min', 'A war begins to brew between man and ape in this sequel to the 2011 hit Rise of the Planet of the Apes. It\'s been ten years since the Simian Flu wiped out most of humanity, and somewhere deep in the woods outside of San Francisco, Caesar (voice and performance capture by Andy Serkis) and his primate companions have established a thriving village built on the principles of peace and community. Shortly after welcoming a baby brother into the family, Caesar\'s son Blue Eyes (Nick Thurston) is walking through the forest with his friend Ash (Doc Shaw) when they cross paths with a human named Carver (Kirk Acevedo), who impulsively draws his gun and shoots Ash at the first sign of aggression. As it turns out, Carver is part of a human expedition led by Malcolm (Jason Clarke), who, along with the rest of his crew, races to Carver\'s side just as Caesar and the rest of the apes answer Blue Eyes\' desperate call for help. An enraged Caesar drives the humans away after realizing they are no longer a threat, and decides to dispatch a small crew to follow them rather than yield to the pleas of his aggressive advisor Koba (Toby Kebbell) to launch an all-out attack. Instead, he decides to show the apes\' strength by amassing outside of the humans\' makeshift community at the base of an unfinished tower, making it unmistakably clear that the two species should remain apart.Meanwhile, the point of the human excursion was to get a dormant dam running again in order to power their community, which will soon be thrust into darkness should they fail to take action. Convinced that he could strike a truce with Caesar that would allow the humans to repair the dam, which is located on the apes\' land, Malcolm gets permission from human leader Dreyfus (Gary Oldman) to set out on his mission. Incredibly, thanks to the help of his girlfriend Ellie (Keri Russell), his son Alexander (Kodi Smit-McPhee), and a few other key allies, Malcolm succeeds. Still, his truce with the apes is a fragile one, and just when it seems that the primates and humans have learned to coexist in peace, a shocking act of betrayal threatens to spark a war that will determine the dominant species.', 1, 0, 2014, NULL);
INSERT INTO `mydb`.`Movie` (`Movie_Id`, `name`, `rating`, `type`, `duration`, `synopsis`, `is3D`, `is2D`, `year`, `awards`) VALUES (5, 'PLANES: FIRE AND RESCUE', 'PG-13', 'Action/Adventure', '1 hr 30 min', 'Adventure flies higher than ever before in this sequel that finds championship air-racer Dusty Crophopper (voice of Dane Cook) beginning an exciting new career fighting forest fires. In the wake of another successful racing season, Dusty returns home to Propwash Junction to resume his training. But a debilitating injury forces Rusty to consider a new career path. Before long Rusty is training with the Aerial Fire Fighters at Piston Peak Air Attack Base. Becoming a Single Engine Air Tanker (SEAT) is no simple task for an injured plane, but with the guidance of seasoned fire-and-rescue helicopter Blade Ranger (voice of Ed Harris), fearless leader of the Piston Peak Air Attack team, Rusty is soon soaring through the clouds once again. Meanwhile, as Rusty gets up to speed, his presence in the unit catches the attention of Dipper (voice of Julie Bowen), a cargo hauler-turned-water scooper capable of skimming lake water to combat raging fires. A major racing fan, Dipper can hardly contain her excitement at the prospect of working alongside Rusty, and meanwhile wise, heavy-lift helicopter Windlifter (voice of Wes Studi) has never encountered a fire he couldn\'t conquer. In no time Rusty has made a whole host of new friends at Piston Peak, including hulking transport plane Cabbie (voice of Dale Dye), the brave grounded firefighters known as the Smokejumpers, the Secretary of the Interior (voice of Fred Willard), and Nick Lopez (voice of Erik Estrada), a helicopter cop with an illustrious past. And though learning the secrets to dousing forest fires is sure to take time, Rusty is a fast learner, and eager to pitch in.', 1, 1, 2014, 'null');

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`Theatre`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`Theatre` (`Theatre_Id`, `name`, `zipcode`, `city`, `state`, `hasIMAX`) VALUES (1, 'AMC Saratoga', '95130', 'San Jose', 'CA', 1);
INSERT INTO `mydb`.`Theatre` (`Theatre_Id`, `name`, `zipcode`, `city`, `state`, `hasIMAX`) VALUES (2, 'AMC Eastridge', '95122', 'San Jose', 'CA', 1);
INSERT INTO `mydb`.`Theatre` (`Theatre_Id`, `name`, `zipcode`, `city`, `state`, `hasIMAX`) VALUES (3, 'Century 20 Oakridge', '95122', 'San Jose', 'CA', 1);
INSERT INTO `mydb`.`Theatre` (`Theatre_Id`, `name`, `zipcode`, `city`, `state`, `hasIMAX`) VALUES (4, 'Cinelux Plaza theatre', '95008', 'Campbell', 'CA', 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`Movie_has_Theatre`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`Movie_has_Theatre` (`Movie_Movie_Id`, `Theatre_Theatre_Id`, `showtimes`, `is3D`) VALUES (movie.movie_id, NULL, NULL, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `mydb`.`Artist`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (1, 'Scarlet Johansson', '35', 'Lead actor', 'F', 'Israeli');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (2, 'Morgan Freeman', '60', 'Lead actor', 'M', 'American');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (3, 'Benicio Del Toro', '30', 'Lead actor', 'M', 'Mexican');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (4, 'Chris Pratt', '30', 'Lead actor', 'M', 'American');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (5, 'Zoe Saldana', '25', 'actor', 'F', 'American');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (6, 'Ophelia lovibond', '25', 'actor', 'F', 'American');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (7, 'Dwayne Johnson', '40', 'Lead actor', 'M', 'American');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (8, 'Ian McShane', '40', 'Lead actor', 'M', 'Australian');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (9, 'Rufus Sewell', '30', 'Lead actor', 'M', 'British');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (10, 'John Hurt', '35', 'Lead actor', 'M', 'British');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (11, 'Rebecca Ferguson', '35', 'Lead actor', 'F', 'American');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (12, 'Gary Oldman', '40', 'Lead actor', 'M', 'American');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (13, 'Keri Russel', '40', 'actor', 'M', 'American');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (14, 'Judy Greer', '35', 'Lead actor', 'F', 'American');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (15, 'Kodi Sit-McPhee', '30', 'actor', 'M', 'American');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (16, 'Dane Cook', '30', 'Lead actor', 'M', 'American');
INSERT INTO `mydb`.`Artist` (`Artist_Id`, `name`, `age`, `role`, `gender`, `nationality`) VALUES (17, 'Julie Bowen', '25', 'Lead actor', 'F', 'American');

COMMIT;

