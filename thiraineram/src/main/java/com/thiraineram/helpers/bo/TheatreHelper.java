/**
 * 
 */
package com.thiraineram.helpers.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.thiraineram.bo.Theatre;
import com.thiraineram.bo.Movie;
import com.thiraineram.helpers.bo.MovieHelper;

/**
 * @author Kamya
 *		08/10/2014
 */
@Service
public class TheatreHelper {
	
	
	private ArrayList<Theatre> theatres = new ArrayList<Theatre>();
	private Map<Movie,ArrayList> showtimeMap = new HashMap<Movie,ArrayList>();
	
	@Autowired
	private MovieHelper movieHelper;
	
	public ArrayList<Theatre> getTheatres(){
		
		
		ArrayList<Movie> movies = movieHelper.getMovies();
		
		ArrayList<String> showtimes1 = new ArrayList<String>();
		showtimes1.add("9:00 AM");
		showtimes1.add("11:00 AM");
		showtimes1.add("2:00 PM");
		showtimes1.add("5:00 PM");
		showtimes1.add("9:00 PM");
		ArrayList<String> showtimes2 = new ArrayList<String>(showtimes1.subList(0,3));
		
		ArrayList<String> showtimes3 = new ArrayList<String>(showtimes1.subList(1 ,4));
		
		
		Theatre theatre1 = new Theatre();
		theatre1.setName("AMC Saratoga");
		theatre1.setCity("SanJose");
		theatre1.setState("CA");
		theatre1.setZipcode(95130);
		theatre1.setHasIMAX(true);
		theatre1.setShowtimes1(showtimes1);
		theatre1.setShowtimes2(showtimes2);
		theatre1.setShowtimes3(showtimes3);
		theatre1.setMovies(movies);
		
		Theatre theatre2 = new Theatre();
		theatre2.setName("AMC Eastridge");
		theatre2.setCity("SanJose");
		theatre2.setState("CA");
		theatre2.setZipcode(95122);
		theatre2.setHasIMAX(true);
		theatre2.setShowtimes1(showtimes1);
		theatre2.setShowtimes2(showtimes2);
		theatre2.setShowtimes3(showtimes3);
		theatre2.setMovies(movies);
		
		Theatre theatre3 = new Theatre();
		theatre3.setName("Century 20 Oakridge");
		theatre3.setCity("SanJose");
		theatre3.setState("CA");
		theatre3.setZipcode(95122);
		theatre3.setHasIMAX(true);
		theatre3.setShowtimes1(showtimes1);
		theatre3.setShowtimes2(showtimes2);
		theatre3.setShowtimes3(showtimes3);
		theatre3.setMovies(movies);
		
		
		Theatre theatre4 = new Theatre();
		theatre4.setName("Cinelux Plaza theatre");
		theatre4.setCity("Campbell");
		theatre4.setState("CA");
		theatre4.setZipcode(95008);
		theatre4.setHasIMAX(false);
		theatre4.setShowtimes1(showtimes1);
		theatre4.setShowtimes2(showtimes2);
		theatre4.setShowtimes3(showtimes3);
		theatre4.setMovies(movies);
		
		
		theatres.add(theatre1);
		theatres.add(theatre2);
		theatres.add(theatre3);
		theatres.add(theatre4);
		
		this.setMovieTimeandType();
		
		return theatres;
		
	}

	public void setMovieTimeandType(){
			
		for(Theatre t : theatres){
			for(Movie m : t.getMovies()){
				if(m.getName() == "LUCY")
					showtimeMap.put(m,t.getShowtimes1());
				if(m.getName() == "GUARDIANS OF THE GALAXY")
					showtimeMap.put(m,t.getShowtimes1());
				if(m.getName() == "HERCULES")
					showtimeMap.put(m,t.getShowtimes2());
				if(m.getName() == "DAWN OF THE PLANET OF THE APES")
					showtimeMap.put(m,t.getShowtimes1());
				if(m.getName() == "PLANES: FIRE AND RESCUE")
					showtimeMap.put(m,t.getShowtimes3());
				}
			
			t.setMovieShowtimeMap(showtimeMap);
			//System.out.println(t.getMovieShowtimeMap());
		}
		
		
		
	}
	
}
