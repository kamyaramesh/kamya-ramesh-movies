/**
 * 
 */
package com.thiraineram.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.internal.CriteriaImpl.CriterionEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.thiraineram.bo.Movie; 
import com.thiraineram.bo.Theatre;
import com.thiraineram.entity.Artist;
import com.thiraineram.entity.MovieEntity;
import com.thiraineram.helpers.bo.MovieHelper;
import com.thiraineram.helpers.bo.TheatreHelper;
import com.thiraineram.entity.TheatreEntity;
import com.thiraineram.service.Search;

/**
 * @author Kamya
 * 08/11/2014
 *
 */
@Service("search")
public class SearchImpl implements Search {

	/* (non-Javadoc)
	 * @see com.thiraineram.service.Search#searchMovieByName(java.lang.String)
	 */
	private Map<String, Movie> moviesMap = new HashMap<String, Movie>();
	private Map<String, Theatre> theatresMap = new HashMap<String, Theatre>();
	private Map<String, ArrayList<Theatre>> zipcodeMap = new HashMap<String, ArrayList<Theatre>>();
	private ArrayList<Movie> movies;
	private ArrayList<Theatre> theatres; 
	@Autowired
	private MovieHelper movieHelper ;
	@Autowired
	private TheatreHelper theatreHelper ;
	
	private ArrayList<Theatre> theatresByZipcode;

	//private SessionFactory sessionFactory ;
	private Configuration configuration ;
    
    private StandardServiceRegistryBuilder ssrb ;
    private SessionFactory sessionFactory ;
    private Session session;
	
	/*
	 * Populates theatres and movies list with the help of helpers. A constructor is not used 
	 * as it creates cyclic dependency and Spring is not able to instantiate a 'Search' bean.
	 */
	public  void getTheatresandMovies(){
		
		theatres = theatreHelper.getTheatres();
		movies = movieHelper.getMovies();
	}
	
	private void openConnection() throws Exception {
		
		configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
        ssrb = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        sessionFactory = configuration.buildSessionFactory(ssrb.build());
        session = sessionFactory.openSession();
		
	}
	
	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	/*
	 * @param name of a movie
	 * @return movie details like rating, cast and synopsis
	 */
	public List searchMovieByName(String movieName) {
		// TODO Auto-generated method stub
		List<MovieEntity> l = null ;
		try{
			this.openConnection();
		session.beginTransaction();
		Query q = session.createSQLQuery("select * from movie m where m.name = :name" )
				.addEntity(MovieEntity.class)
				.setParameter("name", movieName);
		
		l = q.list();
		for(MovieEntity me : l){
			System.out.println(me.getName()+" "+me.getType());
		}	
		//Criteria crit = session.createCriteria(MovieEntity.class);
		session.getTransaction().commit();
		}catch(Exception e){ 
			e.printStackTrace();
		}finally{
			session.close();
		}
		
		return l;
		// Old code starts... no DB
		/*this.getTheatresandMovies();
		this.createMoviesMap();
		
			if(moviesMap.containsKey(movieName)){
				System.out.println(moviesMap.get(movieName).getName()+" " +moviesMap.get(movieName).getRating()+ " "+
				moviesMap.get(movieName).getDuration());
				System.out.println(moviesMap.get(movieName).getType());
				System.out.println(moviesMap.get(movieName).getCast());
				System.out.println(moviesMap.get(movieName).getSynopsis());
				
			return moviesMap.get(movieName);
			
			}
			else
				System.out.println("No matches found");		
		
		return null;
		*/
	}
	
	
	/*This  method creates a map with movie name as key and movie object as value.
	 * Its used by the search by movie name ()
	 * 
	 */
	private void createMoviesMap(){
		
		for (Movie m : movies ){
			moviesMap.put(m.getName(),m);
		}
	}

	/*
	 * @see com.thiraineram.service.Search#searchByZipcode(int)
	 */	
	
	public ArrayList<Theatre> getTheatresByZipcode() {
		return theatresByZipcode;
	}



	public void setTheatresByZipcode(ArrayList<Theatre> theatresByZipcode) {
		this.theatresByZipcode = theatresByZipcode;
	}

	/* Searches for theatres by zipcode
	 * @param zipcode
	 * @return list of theatres
	*/
	@Override
	public ArrayList<Theatre> searchByZipcode(String zipcode) {
		// TODO Auto-generated method stub
		
		//this.SearchImplinitialize();
		this.getTheatresandMovies();
		this.createTheatresMap();
		this.createZipcodeMap();
		if(zipcodeMap.containsKey(zipcode)){
			theatresByZipcode = zipcodeMap.get(zipcode);
			for(Theatre test : theatresByZipcode ){
				System.out.println(test.getName()+" "+test.getZipcode());
				ArrayList<Movie> movies = test.getMovies();
				for(Movie m : movies){
					System.out.println(m.getName());
					ArrayList<String> showtimes = test.getMovieShowtimeMap().get(m);
					System.out.println(test.getMovieShowtimeMap().get(m));
				}
				
				
			}
		return theatresByZipcode;
		
		}else{
			System.out.println("No matches found");
		}
		
	return null;
	}

	/* Searches for theatres by city and state
	 * @param city,state
	 * @return list of theatres
	 * 
	 */
	@Override
	public ArrayList<Theatre> searchByCityState(String CityState) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void createTheatresMap(){

		for(Theatre t : theatres){
			theatresMap.put(t.getName(), t);
		}
	}
	
	/*This creates a map with a zipcode as key and an 'arraylist of theatres' in the zipcode
	 * as objects.
	 * @return map
	 */
	
	private void createZipcodeMap(){
		
		/*ArrayList<Theatre> t1 = new ArrayList<Theatre>();
		for(Theatre t : theatres){
			
			
			for(Theatre test : t1){
				System.out.println(test.getName()+ ":"+test.getZipcode());
			}
			if(zipcodeMap.containsKey(t.getZipcode())){
				zipcodeMap.get(t.getZipcode()).add(t);
			}else{
				t1.add(t);
				zipcodeMap.put(t.getZipcode(), t1);
				t1.clear();
			}
			
		}*/
		
		for(Theatre t : theatres){
			if(zipcodeMap.containsKey((Integer.toString(t.getZipcode())))){
				zipcodeMap.get((Integer.toString(t.getZipcode()))).add(t);
			}else{
				ArrayList<Theatre> t1 = new ArrayList<Theatre>();
				t1.add(t);
				zipcodeMap.put((Integer.toString(t.getZipcode())), t1);
			}
		}
	}
	
	private void createCityStateMap(){
		
	}

}
