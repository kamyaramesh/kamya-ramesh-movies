/**
 * 
 */
package com.thiraineram.bo;

import java.util.ArrayList;
import java.util.Map;

import com.thiraineram.bo.Movie;

/**
 * @author Kamya
 * 08/09/2014
 *
 */
public class Theatre {
	
	private String name;
	private int zipcode;
	private String city;
	private String state;
	private ArrayList<String> locations;
	private ArrayList<String> showtimes1;
	private ArrayList<String> showtimes2;
	private ArrayList<String> showtimes3;
	private boolean hasIMAX;
	private boolean is3D;
	private boolean is2D;
	private ArrayList<Movie> movies;
	private Movie movie;
	private Map<Movie,ArrayList> movieShowtimeMap;
	
	
	public Map<Movie, ArrayList> getMovieShowtimeMap() {
		return movieShowtimeMap;
	}
	public void setMovieShowtimeMap(Map<Movie, ArrayList> movieShowtimeMap) {
		this.movieShowtimeMap = movieShowtimeMap;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	public ArrayList<Movie> getMovies() {
		return movies;
	}
	public void setMovies(ArrayList<Movie> movies) {
		this.movies = movies;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public ArrayList<String> getLocations() {
		return locations;
	}
	public void setLocations(ArrayList<String> locations) {
		this.locations = locations;
	}
	public ArrayList<String> getShowtimes1() {
		return showtimes1;
	}
	public void setShowtimes1(ArrayList<String> showtimes1) {
		this.showtimes1 = showtimes1;
	}
	
	public ArrayList<String> getShowtimes2() {
		return showtimes2;
	}
	public void setShowtimes2(ArrayList<String> showtimes2) {
		this.showtimes2 = showtimes2;
	}
	public ArrayList<String> getShowtimes3() {
		return showtimes3;
	}
	public void setShowtimes3(ArrayList<String> showtimes3) {
		this.showtimes3 = showtimes3;
	}
	public boolean isIs3D() {
		return is3D;
	}
	public void setIs3D(boolean is3d) {
		is3D = is3d;
	}
	public boolean isIs2D() {
		return is2D;
	}
	public void setIs2D(boolean is2d) {
		is2D = is2d;
	}
	public boolean isHasIMAX() {
		return hasIMAX;
	}
	public void setHasIMAX(boolean hasIMAX) {
		this.hasIMAX = hasIMAX;
	}

}
