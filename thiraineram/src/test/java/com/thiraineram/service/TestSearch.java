/**
 * 
 */
package com.thiraineram.service;

import java.util.ArrayList;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.thiraineram.bo.Theatre;
import com.thiraineram.service.impl.SearchImpl;

/**
 * @author Kamya
 *
 */
@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class TestSearch extends AbstractJUnit4SpringContextTests {
	
	@Autowired
	private Search search;
	
	private ArrayList<Theatre> theatresByZipcode;

	
	public void testSearchByMovieName(){
		
		theatresByZipcode =  search.searchByZipcode("95122");
		
		Assert.assertEquals(theatresByZipcode, search.searchByZipcode("95122"));
		//Assert.assertEquals( null,si.searchByZipcode("95123"));
		//Assert.assertEquals(si.getTheatresByZipcode().equals(2), si.searchByZipcode("95122"));
		//Assert.assertEquals("AMC Eastridge", si.getTheatresByZipcode().iterator().next().getName(),  si.searchByZipcode("95122"));
		//Assert.assertEquals("Century 20 Oakridge", si.getTheatresByZipcode().iterator().next().getName(),  si.searchByZipcode("95122"));
		//Assert.assertEquals("Cinelux Plaza theatre", si.getTheatresByZipcode().iterator().next().getName(),  si.searchByZipcode("95008"));
		
	}
	
	public static void main(String[] args){
		
		TestSearch ts = new TestSearch();
		
		ts.testSearchByMovieName();
		
	}
	
	
	
}
	
	


