/**
 * 
 */
package com.thiraineram.factory.bo;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.thiraineram.entity.*;
import com.thiraineram.bo.Movie;

/**
 * @author Kamya
 * 08/19/2014
 *
 */

public class MovieFactory {
	
	
	private SessionFactory sessionFactory;
	
	public ArrayList<Movie> getMovies(){
		
		ArrayList<Movie> movies ;
		
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(MovieEntity.class);
		 
		movies = (ArrayList<Movie>)crit.list();
		
		return movies;
		
	}
	
}
