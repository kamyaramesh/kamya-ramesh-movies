/**
 * 
 */
package com.thiraineram.service;

import java.util.ArrayList;





import java.util.List;

import com.thiraineram.bo.Movie;
import com.thiraineram.bo.Theatre;
/**
 * @author Kamya
 * 08/11/2014
 *
 */
public interface Search {

	List searchMovieByName(String movieName);
	ArrayList<Theatre> searchByZipcode(String zipcode);
	ArrayList<Theatre> searchByCityState(String CityState);
		
	
}
